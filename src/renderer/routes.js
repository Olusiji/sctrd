import VueRouter from 'vue-router';

import job from "./app/job/JobMain"
import task from "./app/task/TaskMain"
import list from "./app/job/JobList"

const routes = [
    {
        path : "/",
        component : job
    },
    {
        path : "/job",
        component : job
    },
    {
        path : "/list",
        component : list
    },
    {
        name : "task",
        path : "/task/:list",
        component : task,
        props : true
    },
    {
        path: '*',
        redirect: '/'
    }
]

export default new VueRouter({

    routes,

    linkActiveClass: "active"

});