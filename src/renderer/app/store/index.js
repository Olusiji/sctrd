import { deleteRecursionList,deleteRecursion } from "./externalfunctions";

let state = {
    job : [],
    list : [],
    tasks : []
};

const getters = {
    getTasks: (state) => state.tasks.slice().reverse(),
    getTask : (state)=> keyword => state.tasks.filter(item =>{
            return item.id === keyword
    }),
    getJobs: (state) => state.job.slice().reverse(),
    getlists: (state) => state.list.slice().reverse()
};

const mutations = {
    UPDATE_STATE(state, {job, list, tasks}){
        state.job.push(...job);
        state.list.push(...list);
        state.tasks.push(...tasks)
    },
    ADD_TASKS(state, payload){
        state.tasks.push(payload)
    },
    UPDATE_TASK(state, {index , payload}){
        state.tasks[index].name = payload;
    },
    DELETE_TASK(state, id){
        state.tasks.forEach((listItem, index, arr)=>{
            if(listItem.id === id) arr.splice(index,1)
        })
    },
    UPDATE_TASK_STATUS(state, {index , status}){
        state.tasks[index].status = status

    },
    UPDATE_TASK_TIME(state, {index , time}){
        state.tasks[index].time = time
    },
    UPDATE_MEDIA(state, {index , status}){
        state.tasks[index].mediaState =  status
    },
    ADD_JOB(state, payload){
        state.job.push(payload)
    },
    UPDATE_JOB(state, {index , payload}){
        state.job[index].name = payload;
    },
    DELETE_JOB(state, id){
        //delete all tasks of the given job_id
        deleteRecursion(state.tasks, state.tasks.length, id);
        //delete all lists of the given job_id
        deleteRecursion(state.list, state.list.length, id);

        state.job.forEach((jobItem, index, arr)=>{
            if(jobItem.id === id) arr.splice(index,1)
        })
    },
    ADD_LIST(state, payload){
        state.list.push(payload)
    },
    UPDATE_LIST(state, {index , payload}){
        state.list[index].name = payload;
    },
    DELETE_LIST(state, id){
        //delete all tasks of the given list_id
        deleteRecursionList(state.tasks, state.tasks.length, id);

        state.list.forEach((listItem, index, arr)=>{
            if(listItem.id === id) arr.splice(index,1)
        })
    }
};

const actions = {
    addTask({ commit }, payload){
        commit('ADD_TASKS', payload);
        //save state to storage
        this.dispatch('saveState');
    },
    updateTask({ commit }, {index, payload}){
        commit('UPDATE_TASK', {index, payload});
        //save state to storage
        this.dispatch('saveState');
    },
    deleteTask({ commit }, id){
        commit('DELETE_TASK', id)
        this.dispatch('saveState')
    },
    updateStatus({ commit }, {index , status}){
        commit('UPDATE_TASK_STATUS', {index , status});
        if(status === "todo") commit('UPDATE_MEDIA', {index, status:"play"})
    },
    updateTime({ commit }, {index, time}){
        commit('UPDATE_TASK_TIME', {index , time})
    },
    updateMedia({ commit }, {index, status}) {
        commit('UPDATE_MEDIA', {index, status})
    },
    addJob({ commit }, payload){
        commit('ADD_JOB', payload);
        //save state to storage
        this.dispatch('saveState');
    },
    updateJob({ commit }, {index, payload}){
        commit('UPDATE_JOB', {index, payload});
        //save state to storage
        this.dispatch('saveState');
    },
    addList({ commit }, payload){
        commit('ADD_LIST', payload);
        //save state to storage
        this.dispatch('saveState');
    },
    updateList({ commit }, {index, payload}){
        commit('UPDATE_LIST', {index, payload});
        //save state to storage
        this.dispatch('saveState');
    },
    deleteList({ commit }, id){
        commit('DELETE_LIST', id)
        this.dispatch('saveState')
    },
    deleteJob({ commit }, id){
        commit('DELETE_JOB', id)
        this.dispatch('saveState')
    },
    saveState(){
        storage.set('state', state);
    },
    updateState({ commit }, payload){
        commit('UPDATE_STATE', payload)
    }
};

export default{
    state,
    getters,
    mutations,
    actions
}

