function updateIds(itemArr, staticLength) {
    //instantiate dynamic length
    let dynamicLength =  staticLength;

    //for each item decrease the id
    itemArr.forEach((item)=> {
        item.id = staticLength - dynamicLength;
        dynamicLength--
    })
}

function updateRelatedIds(itemArr, id){
    itemArr.forEach((item)=>{
        if(item.job_id !== id) --item.job_id
    })
}

function deleteRecursion(arr, length, id) {
    //return early if array is looped through
    if(length<0) return;

    //loop through array
    let newLength = --length;
    //skip when array is looped through
    if(newLength !== -1){
        if(arr[newLength].job_id === id){
            arr.splice(newLength,1)
        }
    }

    deleteRecursion(arr, newLength, id)
}

function deleteRecursionList(arr, length, id) {
    //return early if array is looped through
    if(length<0) return;

    //loop through array
    let newLength = --length;
    //skip when array is looped through
    if(newLength !== -1){
        if(arr[newLength].list_id === id){
            arr.splice(newLength,1)
        }
    }

    deleteRecursionList(arr, newLength, id)
}

export { deleteRecursionList,deleteRecursion }

/*
DELETE_JOB(state, id){
    //delete all tasks of the given job_id
    deleteRecursion(state.tasks, state.tasks.length, id);
    //delete all lists of the given job_id
    deleteRecursion(state.list, state.list.length, id);

    state.job.splice(id,1);
    //re-index the ids after deletion
    updateIds(state.job, state.job.length);
    //update related indexes in tasks and list after deletion
    updateRelatedIds(state.list, id);
    updateRelatedIds(state.tasks, id);
}*/
