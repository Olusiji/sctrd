import Vue  from "vue"
import App from "./app/App.vue"
import Vuex from "vuex"
import store from "./app/store/index"
import Storage from "electron-store"
import VueRouter from "vue-router";
import router from "./routes"
import quickMenu from 'vue-quick-menu'
import FontAwesomeIcon from '@fortawesome/vue-fontawesome'
import "./stylesheet.css"
import 'bulma/css/bulma.css'

import Timer from 'easytimer.js'
const storage = new Storage();

window.storage = storage;
window.Timer = Timer;
window.Vue = Vue;
window.Vuex = Vuex;


Vue.use(Vuex);
Vue.use(VueRouter);

Vue.component('quickMenu',quickMenu);
Vue.component('FontAwesomeIcon',FontAwesomeIcon);

new Vue({
    el: '#app',
    router,
    store: new Vuex.Store(store),
    render: h => h(App)
});